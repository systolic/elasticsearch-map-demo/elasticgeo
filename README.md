# SYSTOLIC ElasticGeo Proof-of-Concept

This project is intended to demonstrate how ElasticSearch + GeoServer + ElasticGeo can render interactive maps from 
data sources containing millions of records. Any data which can be ingested into ElasticSearch and contains geographic
coordinates (lat, lon) can be visualized. Real-time filtering is fully supported, as is the ability to select
individual points to view information contained within the matching ElasticSearch documents.

An open source cell tower dataset from [OpenCellID](https://opencellid.org) is used for this proof-of-concept. This dataset contains 
41.5 Million records. The client is built as a "cell tower browser", including dynamic filtering of several
tower-related fields and inspection of individual tower details. The goal is to demonstrate the core capabilities
of the ElasticSearch + GeoServer + ElasticGeo stack using a concrete example.

![Screenshot](screenshot.png)

![Screenshot](screenshot-2.png)

![Screenshot](screenshot-3.png)

## Purpose
SYSTOLIC has encountered customer requests for geographic visualization solutions that can render millions or even 
billions of points in a web application with interactive performance. Traditional visualizations send the data to the
client before rendering the visualization in the browser, and are typically limited to thousands of points before
exhausting the resources of the client. Sometimes, though, customers have large datasets with millions of points,
and desire the ability to see the "big picture" of those datasets all at once. These big picture visualizations can
reveal patterns or trends in the data that are important to the customer.

The idea for this solution originated from a hacked experiment I wrote while playing with different mapping solutions.
I was aware of this customer need and wanted to see what existed in the open source world. I knew that I was looking for
solutions that rendered map tile images on the server and delivered those images to the client instead of the
raw data points. This approach leverages the significant work already done to support mapping applications like
Google Maps, but for data layers instead of the geographic layer. I expected to find that I could use libraries
to write my own server that would render map tile images from arbitrary data. I was somewhat surprised to find that
not only was such a server already available, but that it leveraged ElasticSearch as a data source. This is particularly
significant because ElasticSearch is so widely used by our customers. In a few hours, I had configured a server to
render map tiles for data from an ElasticSearch index, and written a simple client that displayed that data on a map.

## Approach
GeoServer is a popular mapping server written in Java. It is a mature product that supports a wide variety of mapping
capabilities. One of those core capabilities is the ability to render map tiles using the industry standard WMS
(Web Map Service) API. Typically this is used to render static map layers from data files. This is useful but does not
address the problem of rendering map layers from dynamic data. Fortunately the National Geospatial-Intelligence Agency
(NGA) built a GeoServer plugin called ElasticGeo and released it as open source. This plugin seemingly does exactly
what we need - it takes data from ElasticSearch and passes it to GeoServer where it can be rendered as map tiles.
This is all done in user request time, so as the data in ElasticSearch changes, subsequent maps display the new
data. This capability heavily leverages ElasticSearch's powerful aggregation feature. Rather than retrieve potentially
millions of documents from the database, GeoServer asks ElasticSearch to perform an aggregation on the geographic
positions within those documents. ElasticSearch already has great support for doing this. ElasticGeo only
needs to load counts of documents by geographic "bin" from which an image is rendered.

## Advantages
### Free and open source
No license cost means the customer is free to deploy instances as necessary. This supports tactical use cases where the
number of instances is unpredictable. 
### High performance and scalable
Since all computation is done on the server and only tile images are sent to the client, the client can display 
millions of points at once.
### Works with any ElasticSearch data that contains geo points
The fact that this solution operates on the customer's existing ElasticSearch data is appealing. The only requirement
is that the data has a `geo_point` field with a location.
### Does not require any precomputation or derived data
Some solutions require the precomputation of derived results to achieve performance. This solution, however, works 
directly with the customer's existing ElasticSearch indices. To update the map, the customer only needs to update their
ElasticSearch data. This enables real-time filtering of the map visualization using ElasticSearch queries. This also
means that the client can easily query the server for any additional data associated with points using standard
ElasticSearch queries.
### Client library agnostic
This example uses OpenLayers, but the client could be Leaflet, Mapbox GL, or any other client that supports WMS tiles.
OpenLayers was chosen because it includes support for a single tile WMS source, which this approach needs, but
there are examples online for doing the same with Leaflet.

## Disadvantages
### Limited zoom precision
This is probably the biggest limitation I found of this approach. ElasticGeo leverages ElasticSearch's 
[GeoHash grid](https://www.elastic.co/guide/en/elasticsearch/reference/current/search-aggregations-bucket-geohashgrid-aggregation.html)
aggregation. This aggregation is based on a 12-character geohash that is stored with every `geo_point` field.
The number of characters in the geohash determines the precision, so there are only 12 possible levels of precision
to the resulting aggregation. Because these 12 precisions are meant to cover all use cases, they vary widely in
size, from 5000 km^2 for precision 1, to ~3cm^2 for precision 12. Only ~6 of these precisions are of practical
use for our needs. This means that there are only ~6 possible geographic "bucket" sizes that will be used to bin
documents. The map UI, however, allows the user to smoothely zoom into the map, so there is a continuous set of zoom 
levels. As the user zooms into the map, both the underlying base map layer and the ElasticGeo data layer are
recomputed for the new zoom level. The base map layer smoothly transitions to the new zoom level, and the ElasticGeo
data layer tries to as well, but at some point a new aggregation precision will be selected (say from 4 to 5),
at which time the next map will look quite a bit different from the previous map. The result can be jarring
as the user zooms in. The client has complete control over when this transition occurs, and I've attempted to 
choose what I considered to be the best points to make those transitions, but it can still be distracting to the user.
### Performance is good, but not always sub-second
On my local desktop, the application usually feels quite responsive, and in most cases I get sub-second response times.
Sometimes, however, it may take more than a second to for the data to display. This is with a single out-of-the-box 
ElasticSearch instance on a desktop workstation, so it's possible this number may be improved. I don't see this as
a significant disadvantage as overall the application feels pretty responsive. However if we had a customer with
many millions or billions of points, we might want to consider whether architecture changes would be worth the cost.
Profiling would be key, but I could imagine possible improvements from integrating into ElasticSearch as an
ElasticSearch plugin, for example.
### Single-tile support
There **was** a problem where ElasticGeo would always dynamically compute the aggregation precision using settings
which were optimized for fixed tile sizes like usually seen in mapping applications where the map is composed of
many tiles. For our solution, however, it was important to render the layer as a single tile so that we can scale the
data uniformly across the entire map (otherwise each tile looks different). Fortunately I was able to make a patch
to ElasticGeo to support client-provided precisions. I submitted a 
[pull request](https://github.com/ngageoint/elasticgeo/pull/111) to the ElasticGeo project on GitHub,
and it has been accepted. So a future release of ElasticGeo will remove this restriction. For now I'm using my modified
version.
### Memory usage
Out of the box, ElasticSearch sets a limit on the number of buckets that can be returned for an aggregation query.
This limit is insufficient for our needs, where we want approximately one bucket for each pixel in the map image.
This required me to set a global ElasticSearch config (`search.max_buckets`) to a much larger value. This solved the
problem, but means the aggregation uses more memory. I had no issues in my testing, but of course I'm only one user.
It's possible that this may present an issue in cases where there are many simultaneous users.

## Deployment Notes
* ElasticSearch version: 7.5.1
* GeoServer version: 2.16.1
* ElasticGeo version: 2.16.0-RC1 (modified version)
* Created SYSTOLIC Workspace on GeoServer
* Added a new ElasticGeo Store for the cell-towers layer
* Added a new GeoServer Layer for cell-towers
* Added several example styles. See /resources/styles.
* Set search.max_buckets in ElasticSearch to 1,000,000.
* Added "Individual Points" style to demonstrate rendering each individual result instead of aggregated results. default_max_features under Store settings controls max number of documents to return.
* Set CORS config in elasticsearch.yml so client can query directly: `http.cors.enabled: true`, `http.cors.allow-origin: http://localhost:3000`
* Original plan called for multiple data sources to demonstrate multiple layers, however I had trouble finding other sources that complemented OpenCellID and decided it was more important to tell a complete story with this dataset.
* [GeoNames](https://www.geonames.org) is another data source that I tried, and there is code in `importers` to ingest it. Ultimately decided it was not a good fit for this effort.

## nginx server config
    server {
		listen 3000;

		location / {
			root /opt/ui;
		}

		location /cell-towers {
			proxy_pass http://127.0.0.1:9200/cell-towers;
		}

		location /geoserver {
			proxy_pass http://127.0.0.1:8080/geoserver;
		}
	}