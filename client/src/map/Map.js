import _ from 'lodash'
import React, {Component} from 'react'
import View from 'ol/View'
import OpenLayerMap from 'ol/Map'
import ImageLayer from 'ol/layer/Image'
import TileLayer from 'ol/layer/Tile'
import XYZ from 'ol/source/XYZ'
import {fromLonLat} from 'ol/proj'
import {defaults as defaultControls, ScaleLine} from 'ol/control'
import {transform, transformExtent} from 'ol/proj'
import ElasticgeoSingleTileImageSource from './ElasticgeoSingleTileImageSource'
import GeohashPrecisionUtils from '../support/GeohashPrecisionUtils'
import 'ol/ol.css'
import './Map.css'
import Overlay from 'ol/Overlay'

//const baseUrl = 'http://localhost:8080'
const baseUrl = ''

class Map extends Component {

    constructor(props) {
        super(props)
        this.mapContainer = React.createRef()
        this.handleWindowResize = this.onWindowResize.bind(this)
        this.eventuallyResizeMapContainer = _.debounce(this.resizeMapContainer.bind(this), 500)
    }

    handleMoveEnd = (event) => {
        const view = this.state.map.getView()
        const zoom = view.getZoom()
        const precision = GeohashPrecisionUtils.getPrecisionForZoom(zoom)
        const extent = transformExtent(view.calculateExtent(this.state.map.getSize()), view.getProjection().getCode(), 'EPSG:4326')

        this.props.onMapChange(extent, zoom, precision)
    }

    handleClick = (event) => {
        const view = this.state.map.getView()
        const geoPoint = transform(event.coordinate, view.getProjection().getCode(), 'EPSG:4326')
        while (geoPoint[0] < -180) {
             geoPoint[0] = 360 - Math.abs(geoPoint[0])
        }
        const resolution = view.getResolution()

        this.detailsOverlay.setPosition(event.coordinate)

        this.props.onMapClick(geoPoint, resolution)
    }

    handleDataLayerImagerLoadEnd = () => {
        this.updateDataLayerFilter()
    }

    updateDataLayerFilter() {
        this.props.layers.filter(layer => Number.isFinite(layer.hue)).forEach(layer => {
            // TODO support multiple data layers
            const dataLayerEl = document.querySelector('.data-layer')
            if (!dataLayerEl) { return }

            dataLayerEl.style.filter = `hue-rotate(${layer.hue}deg)`
        })
    }

    initializeMap() {
        const layers = this.initializeLayers()

        this.detailsOverlay = new Overlay({
            element: document.getElementById('details-overlay'),
            autoPan: true
        });

        const view = new View({
            center: fromLonLat([-77.03, 38.89]),
            zoom: 2
        })

        const map = new OpenLayerMap({
            target: this.mapContainer.current,
            layers: layers,
            overlays: [this.detailsOverlay],
            view: view,
            controls: defaultControls().extend([
                new ScaleLine()
            ])
        })

        this.setState({
            map: map
        })

        map.on('moveend', this.handleMoveEnd)
        map.on('click', this.handleClick)
    }

    initializeLayers() {
        return this.props.layers.map(layer => {
            if (layer.id === 'baseMap') {
                const baseMapId = 'mapbox.dark'
                const accessToken = 'pk.eyJ1IjoibW9uZ29qb25lcyIsImEiOiJjazMzbm1kd3Ywd2diM21wYXBoOTR5cndkIn0.Mf9Zu307-BSPLhfa88NaFg'
                return new TileLayer({
                    id: layer.id,
                    className: 'base-layer',
                    source: new XYZ({
                        url: `https://api.tiles.mapbox.com/v4/${baseMapId}/{z}/{x}/{y}.png?access_token=${accessToken}`
                    }),
                    opacity: layer.opacity
                })
            } else {
                const imageSource = new ElasticgeoSingleTileImageSource({
                    url: `${baseUrl}/geoserver/wms`,
                    params: this.createLayerParams(layer),
                    ratio: 1,
                    serverType: 'geoserver',
                    aggregationField: 'location',
                    aggregationSize: 100000,
                    getZoom: () => this.state.map.getView().getZoom()
                })
                imageSource.on('imageloadend', this.handleDataLayerImagerLoadEnd)
                return new ImageLayer({
                    id: layer.id,
                    className: 'data-layer',
                    source: imageSource,
                    opacity: layer.opacity
                })
            }
        })
    }

    componentDidMount() {
        this.resizeMapContainer()

        this.initializeMap()

        window.addEventListener('resize', this.handleWindowResize)
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.handleWindowResize)
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        const {onDataLayerUpdate} = this.props

        prevProps.layers.forEach((prevLayer, index) => {
            let layer = this.props.layers[index]
            if (layer.opacity !== prevLayer.opacity) {
                this.getLayerById(prevLayer.id).setOpacity(layer.opacity)
            }
            if (layer.style && layer.style !== prevLayer.style) {
                this.getLayerById(prevLayer.id).getSource().updateParams(this.createLayerParams(layer))
            }
            if (layer.hue && layer.hue !== prevLayer.hue) {
                this.updateDataLayerFilter()
            }
            if (layer.filters && !_.isEqual(layer.query, prevLayer.query)) {
                this.getLayerById(prevLayer.id).getSource().updateQuery(layer.query)
                onDataLayerUpdate()
            }
        })
    }

    onWindowResize() {
        this.eventuallyResizeMapContainer()
    }

    resizeMapContainer() {
        this.mapContainer.current.style.height = `${window.innerHeight}px`
    }

    render() {
        return <div ref={this.mapContainer} className="map-container"></div>
    }

    getLayerById(id) {
        return this.state.map.getLayers().getArray().find(layer => layer.get('id') === id)
    }

    createLayerParams(layer) {
        return {
            'LAYERS': layer.name,
            'STYLES': layer.style
        }
    }

}

export default Map