import ImageWMS from 'ol/source/ImageWMS'
import GeohashPrecisionUtils from '../support/GeohashPrecisionUtils'

class ElasticgeoSingleTileImageSource extends ImageWMS {

    individualPointStyles = ['SYSTOLIC:tower-icons']

    nestedAggregationsByStyle = {
        'SYSTOLIC:country-codes': {
            "mcc": {
                "terms": {
                    "field": "mcc"
                }
            }
        }
    }

    constructor(options) {
        super(options)

        this.aggregationField = options.aggregationField || 'location'
        this.aggregationSize = options.aggregationSize || 100000
        this.getZoom = options.getZoom

        this.query = null
    }

    updateQuery(query) {
        this.query = query
        this.changed();
    }

    /** @override */
    getRequestUrl_(extent, size, pixelRatio, projection, params) {
        const zoom = this.getZoom()
        const precision = GeohashPrecisionUtils.getPrecisionForZoom(zoom)
        const style = params['STYLES']
        const geoserverStyle = this.getGeoserverStyleForLayerStyle(style, precision)

        console.log(`Zoom: ${zoom}, Precision: ${precision}`)

        let extendedParams
        if (!this.individualPointStyles.includes(geoserverStyle) || this.nestedAggregationsByStyle[geoserverStyle]) {
            extendedParams = Object.assign({}, params, {
                'STYLES': geoserverStyle,
                'viewparams': this.createViewParams(geoserverStyle, precision)
            })
        } else {
            if (this.query) {
                extendedParams = Object.assign({}, params, {
                    'STYLES': geoserverStyle,
                    'viewparams': this.encodeQuery(this.query)
                })
            } else {
                extendedParams = Object.assign({}, params, {
                    'STYLES': geoserverStyle
                })
            }
       }

       return super.getRequestUrl_(extent, size, pixelRatio, projection, extendedParams);
    }

    getGeoserverStyleForLayerStyle(layerStyle, precision) {
        let geoserverStyle
        if (layerStyle === 'country-codes') {
            geoserverStyle = 'SYSTOLIC:country-codes'
        } else if (precision >= 9) {
            geoserverStyle = 'SYSTOLIC:tower-icons'
        } else if (precision === 8) {
            geoserverStyle = 'SYSTOLIC:heatmap-circles'
        } else {
            geoserverStyle = 'SYSTOLIC:heatmap'
        }
        return geoserverStyle
    }

    createViewParams(geoserverStyle, precision) {
        let aggregation
        if (!this.query || !this.individualPointStyles.includes(geoserverStyle)) {
            aggregation = {
                agg: {
                    geohash_grid: {
                        field: this.aggregationField,
                        precision: String(precision),
                        size: this.aggregationSize
                    }
                }
            }
            if (this.nestedAggregationsByStyle[geoserverStyle]) {
                Object.assign(aggregation.agg, {
                    aggs: this.nestedAggregationsByStyle[geoserverStyle]
                })
            }
        } else {
            aggregation = null
        }

        if (this.query) {
            console.log(`Query: ${JSON.stringify(this.query)}`)
        }
        if (aggregation) {
            console.log(`Aggregation: ${JSON.stringify(aggregation)}`)
        }

        if (this.query && aggregation) {
            return `${this.encodeQuery(this.query)};${this.encodeAggregation(aggregation)}`
        } else if (this.query) {
            return this.encodeQuery(this.query)
        } else {
            return this.encodeAggregation(aggregation)
        }
    }

    encodeQuery(query) {
        return this.encodeForElasticgeo('q', query)
    }

    encodeAggregation(aggregation) {
        return this.encodeForElasticgeo('a', aggregation)
    }

    encodeForElasticgeo(prefix, value) {
        return `${prefix}:${JSON.stringify(value).replace(/,/g, '\\,')}`
    }

}

export default ElasticgeoSingleTileImageSource
