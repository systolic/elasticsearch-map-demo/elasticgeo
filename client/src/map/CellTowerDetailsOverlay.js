import React, {Component} from 'react'
import Paper from '@material-ui/core/Paper'
import Box from '@material-ui/core/Box'
import Divider from '@material-ui/core/Divider'
import './CellTowerDetailsOverlay.css'

/*
radio: "UMTS"
mcc: 310
net: 410
area: 15904
cell: 208044403
range: 6222
location: {lat: 37.551093, lon: -86.366239}
type: "National"
countryName: "United States of America"
countryCode: "US"
brand: "AT&T"
operator: "AT&T Mobility"
status: "Operational"
bands: "GSM 850 / GSM 1900 / UMTS 850 / UMTS 1900"
 */

function CellTowerDetails({cellTower}) {
    return (
        <Box>
            <Box p={1}>
                <div className="heading">
                    <div className="heading-item">{cellTower.radio}</div>
                    <div className="heading-item">MCC {cellTower.mcc}</div>
                    <div className="heading-item">MNC {cellTower.net}</div>
                    <div className="heading-item">Area {cellTower.area}</div>
                </div>
                <dl>
                    <dt>Location</dt>
                    <dd>{cellTower.location.lat}, {cellTower.location.lon}</dd>
                    <dt>Country</dt>
                    <dd>{cellTower.countryName || '--'}</dd>
                    <dt>Operator</dt>
                    <dd>{cellTower.operator || '--'}</dd>
                    <dt>Status</dt>
                    <dd>{cellTower.status || '--'}</dd>
                </dl>
            </Box>
            <Divider/>
        </Box>
    )
}

class CellTowerDetailsOverlay extends Component {

    render() {
        const {cellTowers} = this.props
        const classNames = ['details-overlay']

        let cellTowerDetails = []
        if (cellTowers && cellTowers.length > 0) {
            cellTowerDetails = cellTowers.map(cellTower => <CellTowerDetails key={cellTower.id} cellTower={cellTower}/>)
        } else {
            classNames.push('hidden')
        }

        return (
            <Paper id="details-overlay" className={classNames.join(' ')}>
                <Box className="details-overlay-content">
                    {cellTowerDetails}
                </Box>
            </Paper>
        )
    }

}

export default CellTowerDetailsOverlay
