import _ from 'lodash'
import React, {Component} from 'react'
import Box from '@material-ui/core/Box'
import Drawer from '@material-ui/core/Drawer'
import Typography from '@material-ui/core/Typography'
import withStyles from '@material-ui/core/styles/withStyles'
import Map from 'map/Map'
import CellTowerDetailsOverlay from 'map/CellTowerDetailsOverlay'
import LayersPanel from 'sidebar/LayersPanel'
import ElasticsearchStatsPanel from 'sidebar/ElasticsearchStatsPanel'
import './App.css'

//const baseUrl = 'http://localhost:9200'
const baseUrl = ''

const drawerWidth = 240

const styles = (theme) => {
    return {
        root: {
            display: 'flex'
        },
        drawer: {
            width: drawerWidth
        },
        drawerPaper: {
            width: drawerWidth
        },
        content: {
            width: `calc(100% - ${drawerWidth}px)`,
            marginLeft: drawerWidth
        }
    }
}

function TitlePanel({documentCount}) {
    return (
        <Box mt={1} mb={1}>
            <Typography variant="h5" align="center">Cell Towers</Typography>
            {_.isNumber(documentCount) &&
            <Typography variant="h6" align="center">{documentCount.toLocaleString()}</Typography>
            }
        </Box>
    )
}

class App extends Component {

    constructor(props) {
        super(props)
        this.overlayRef = React.createRef()
        this.eventuallyUpdateDocumentCount = _.debounce(this.updateDocumentCount.bind(this), 500)
        this.state = {
            layers: [{
                id: 'baseMap',
                label: 'Base Map',
                opacity: 0.5
            }, {
                id: 'cell-towers',
                name: 'SYSTOLIC:cell-towers',
                label: 'Cell Tower Layer',
                opacity: 1,
                style: 'heatmap',
                hue: 0,
                filters: {
                    'radio': {
                        availableValues: ['CDMA', 'GSM', 'LTE', 'UMTS'],
                        currentValues: ['CDMA', 'GSM', 'LTE', 'UMTS']
                    },
                    'status': {
                        availableValues: ['Allocated', 'Not operational', 'Operational', 'Reserved', 'Unknown', 'Temporary operational'],
                        currentValues: ['Allocated', 'Not operational', 'Operational', 'Reserved', 'Unknown', 'Temporary operational']
                    },
                    'range': {
                        availableValues: [0, 50000],
                        currentValues: [0, 50000]
                    }
                },
                query: this.createQuery()
            }],
            extent: undefined,
            zoom: undefined,
            precision: undefined,
            documentCount: 0,
            selectedDocuments: undefined
        }
    }

    createQueryClauseToMatchValues(fieldName, values) {
        return {
            'bool': {
                'should': values.map(value => {
                    return {
                        'match': {
                            [fieldName]: value
                        }
                    }
                })
            }
        }
    }

    createQueryClauseToMatchRange(fieldName, min, max) {
        return {
            'range': {
                [fieldName]: {
                    'gte': min,
                    'lte': max
                }
            }
        }
    }

    createQuery(layer) {
        if (!layer) {
            return null
        }

        const clauses = []
        const {radio, status, range} = layer.filters

        if (radio.currentValues.length < radio.availableValues.length) {
            clauses.push(this.createQueryClauseToMatchValues('radio', radio.currentValues))
        }
        if (status.currentValues.length < status.availableValues.length) {
            clauses.push(this.createQueryClauseToMatchValues('status', status.currentValues))
        }
        if (range.currentValues[0] > range.availableValues[0] || range.currentValues[1] < range.availableValues[1]) {
            clauses.push(this.createQueryClauseToMatchRange('range', range.currentValues[0], range.currentValues[1]))
        }

        if (clauses.length > 0) {
            return {
                'bool': {
                    'must': [clauses]
                }
            }
        } else {
            return null
        }
    }

    handleLayerOpacityChange = (layer, opacity) => {
        this.setState({
            layers: Object.assign([], this.state.layers, {[this.state.layers.indexOf(layer)]: {...layer, opacity}})
        })
    }

    handleLayerStyleChange = (layer, style) => {
        this.setState({
            layers: Object.assign([], this.state.layers, {[this.state.layers.indexOf(layer)]: {...layer, style}})
        })
    }

    handleLayerHueChange = (layer, hue) => {
        this.setState({
            layers: Object.assign([], this.state.layers, {[this.state.layers.indexOf(layer)]: {...layer, hue}})
        })
    }

    handleLayerFilterChange = (layer, filterType, value) => {
        const newLayer = {...layer}
        newLayer.filters = {...newLayer.filters}

        if (filterType === 'radio' || filterType === 'status' || filterType === 'range') {
            const selectedValues = value
            newLayer.filters[filterType] = {...(newLayer.filters[filterType])}
            newLayer.filters[filterType].currentValues = selectedValues
        }

        newLayer.query = this.createQuery(newLayer)

        this.setState({
            layers: Object.assign([], this.state.layers, {[this.state.layers.indexOf(layer)]: newLayer})
        })
    }

    handleDataLayerUpdate = () => {
        this.eventuallyUpdateDocumentCount()
    }

    handleMapChange = (extent, zoom, precision) => {
        this.setState({
            extent: extent,
            zoom: zoom,
            precision: precision
        })
    }

    handleMapClick = (geoPoint, resolution) => {
        this.updateSelectedGeoPoint(geoPoint, resolution)
    }

    componentDidMount() {
        this.eventuallyUpdateDocumentCount()
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.extent !== prevState.extent) {
            this.eventuallyUpdateDocumentCount()
        }
    }

    render() {
        const {classes} = this.props
        const {documentCount, selectedDocuments} = this.state

        return (
            <div className="app">
                <Drawer
                    className={classes.drawer}
                    classes={{
                        paper: classes.drawerPaper
                    }}
                    variant="permanent"
                    anchor="left">
                    <TitlePanel documentCount={documentCount}/>
                    <LayersPanel layers={this.state.layers}
                                 onLayerOpacityChange={this.handleLayerOpacityChange}
                                 onLayerStyleChange={this.handleLayerStyleChange}
                                 onLayerHueChange={this.handleLayerHueChange}
                                 onLayerFilterChange={this.handleLayerFilterChange}/>
                    <ElasticsearchStatsPanel precision={this.state.precision}/>
                </Drawer>
                <main className={classes.content}>
                    <Map layers={this.state.layers} onDataLayerUpdate={this.handleDataLayerUpdate}
                         onMapChange={this.handleMapChange} onMapClick={this.handleMapClick}></Map>
                    <CellTowerDetailsOverlay ref={this.overlayRef} cellTowers={selectedDocuments}/>
                </main>
            </div>
        )
    }

    updateDocumentCount() {
        const layerWithQuery = this.state.layers.find(layer => layer.query)

        let query
        if (layerWithQuery) {
            query = layerWithQuery.query
        } else {
            query = {
                'bool': {
                    'must': {
                        'match_all': {}
                    }
                }
            }
        }

        if (this.state.extent && this.state.zoom > 2.2) {
            query = this.addBoundingBoxFilter(query, this.state.extent)
        }

        fetch(`${baseUrl}/cell-towers/_count`, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'query': query
            })
        })
            .then(response => response.json())
            .then(data => {
                this.setState({
                    documentCount: data.count
                })
            })
    }

    addBoundingBoxFilter(query, extent) {
        let [minX, minY, maxX, maxY] = extent

        while (minX < -180) {
            minX = 360 - Math.abs(minX)
        }
        while (maxX < -180) {
            maxX = 360 - Math.abs(maxX)
        }

        const queryWithFilter = _.clone(query)
        queryWithFilter.bool.filter = {
            'geo_bounding_box': {
                'location': {
                    'bottom_left': [minX, minY],
                    'top_right': [maxX, maxY]
                }
            }
        }
        return queryWithFilter
    }

    updateSelectedGeoPoint(geoPoint, resolution) {
        const [lon, lat] = geoPoint
        const distanceInPixels = 5
        const distance = ((resolution * distanceInPixels) / 1000) + 'km'

        const query = {
            'bool': {
                'must': {
                    'match_all': {}
                },
                'filter': {
                    'geo_distance': {
                        'distance': distance,
                        'location': {
                            'lat': lat,
                            'lon': lon
                        }
                    }
                }
            }
        }

        fetch(`${baseUrl}/cell-towers/_search`, {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                'query': query,
                'size': 100
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data && data['hits'] && data['hits']['hits']) {
                    const selectedDocuments = data['hits']['hits'].map(hit => {
                        return Object.assign({}, {id: hit['_id']}, hit['_source'])
                    })
                    this.setState({selectedDocuments})
                }
            })
    }

}

export default withStyles(styles)(App)
