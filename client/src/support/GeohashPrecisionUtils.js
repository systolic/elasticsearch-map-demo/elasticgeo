const geohashPrecisions = [{
    precision: 4,
    zoomRange: [0, 3]
}, {
    precision: 5,
    zoomRange: [3, 5.5]
}, {
    precision: 6,
    zoomRange: [5.5, 8.5]
}, {
    precision: 7,
    zoomRange: [8.5, 10.7]
}, {
    precision: 8,
    zoomRange: [10.7, 16]
}, {
    precision: 9,
    zoomRange: [16, 100]
}]

class GeohashPrecisionUtils {

    static getPrecisionForZoom(zoom) {
        return geohashPrecisions.find(geohashPrecision => {
            const [minZoom, maxZoom] = geohashPrecision.zoomRange
            return zoom >= minZoom && zoom < maxZoom
        }).precision
    }

}

export default GeohashPrecisionUtils