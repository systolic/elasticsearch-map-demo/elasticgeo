import React, {Component} from 'react'
import Box from '@material-ui/core/Box'

class ElasticsearchStatsPanel extends Component {

    render() {
        const {precision} = this.props

        return (
            <Box p={2}>
                Geohash Precision: {precision ? precision : ''}
            </Box>
        )
    }

}

export default ElasticsearchStatsPanel
