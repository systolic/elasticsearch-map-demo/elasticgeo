import React, {Component} from 'react'
import Box from '@material-ui/core/Box'
import CellTowersLayerEditor from './CellTowersLayerEditor'
import './LayersPanel.css'
import BaseLayerEditor from './BaseLayerEditor'
import Divider from '@material-ui/core/Divider'

class LayersPanel extends Component {

    render() {
        const {layers} = this.props
        const baseLayer = layers[0]
        const dataLayer = layers[1]

        return (
            <Box className="layers-panel">
                <Box mb={1} px={2} py={1}>
                    <BaseLayerEditor layer={baseLayer} onLayerOpacityChange={this.props.onLayerOpacityChange}/>
                </Box>
                <Divider/>
                <Box px={2} py={1}>
                    <CellTowersLayerEditor layer={dataLayer}
                                           onLayerOpacityChange={this.props.onLayerOpacityChange}
                                           onLayerStyleChange={this.props.onLayerStyleChange}
                                           onLayerHueChange={this.props.onLayerHueChange}
                                           onLayerFilterChange={this.props.onLayerFilterChange}/>
                </Box>
                <Divider/>
            </Box>
        )
    }

}

export default LayersPanel