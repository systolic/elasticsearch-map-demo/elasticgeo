'use strict'

const _ = require('lodash')
const fs = require('fs')
const { Client } = require('@elastic/elasticsearch')
const csv = require('fast-csv')
const mcc_mnc_list = require('mcc-mnc-list');

const client = new Client({ node: 'http://localhost:9200' })

let dataset = []
let totalRows = 0
let firstRow = true

const BATCH_SIZE = 10000

const csvFile = '../install/raw-layer-data/cell_towers_2019-11-17-T000000.csv'
const indexName = 'cell-towers'
/*
{
  "mappings": {
    "properties": {
      "radio": {
        "type": "keyword"
      },
      "mcc": {
        "type": "integer"
      },
      "net": {
        "type": "integer"
      },
      "area": {
        "type": "integer"
      },
      "cell": {
        "type": "integer"
      },
      "range": {
        "type": "integer"
      },
      "location": {
        "type": "geo_point"
      },
      "countryCode": {
        "type": "keyword"
      },
      "status": {
        "type": "keyword"
      }
    }
  }
}
 */
const integerFields = ['mcc', 'net', 'area', 'cell', 'range']
const fieldsToExclude = ['type', 'unit', 'lat', 'lon', 'created', 'updated', 'changeable', 'averageSignal', 'samples']
const fieldsToAddFromMccMnc = ['countryName', 'countryCode', 'operator', 'status']

function enrichWithMccMncData(cellTower) {
    const mcc = String(cellTower.mcc).padStart(3, '0')
    const mnc = String(cellTower.net).padStart(2, '0')
    const match = mcc_mnc_list.find({ mccmnc: mcc + mnc })
    if (match) {
        Object.assign(cellTower, _.pick(match, fieldsToAddFromMccMnc))
    }
}

fs.createReadStream(csvFile)
    .pipe(csv.parse({headers: true}))
    .on('error', error => console.error(error))
    .on('data', cellTower => {
        if (firstRow) {
            console.log(`Available fields: ${Object.keys(cellTower).sort().join(', ')}`)
            firstRow = false
        }

        integerFields.forEach(fieldName => cellTower[fieldName] = parseInt(cellTower[fieldName]))
        cellTower.location = {lat: parseFloat(cellTower.lat), lon: parseFloat(cellTower.lon)}
        fieldsToExclude.forEach(fieldName => delete cellTower[fieldName])
        enrichWithMccMncData(cellTower)

        dataset.push(cellTower)

        if (dataset.length % BATCH_SIZE === 0) {
            const body = _.flatMap(dataset, doc => [{index: {_index: indexName}}, doc])

            client.bulk({refresh: true, body})

            totalRows += dataset.length
            dataset = []

            console.log(`Wrote ${BATCH_SIZE} documents. Total written: ${totalRows}`)
        }
    })
    .on('end', rowCount => console.log(`Wrote ${rowCount} total documents.`))
