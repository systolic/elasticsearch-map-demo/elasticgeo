'use strict'

const _ = require('lodash')
const mcc_mnc_list = require('mcc-mnc-list');
const d3 = require('d3-scale-chromatic')

let colorIndex = 0;

_.uniq(mcc_mnc_list.all().map(entry => entry.mcc)).filter(mcc => /^\d\d\d$/.test(mcc)).sort().forEach(mcc => {
    const color = d3.schemeCategory10[colorIndex++]

    console.log(`                            <ColorMapEntry color="${color}" quantity="${mcc}"/>`)

    if (colorIndex >= d3.schemeCategory10.length) {
        colorIndex = 0
    }
})
