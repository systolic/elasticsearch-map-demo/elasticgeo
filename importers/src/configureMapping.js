'use strict'

const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://localhost:9200' })

client.indices.create({
    index: 'cell-towers',
    body: {
        "mappings": {
            "properties": {
                "area": {
                    "type": "long"
                },
                "cell": {
                    "type": "long"
                },
                "countryCode": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "countryName": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "location": {
                    "type": "geo_point"
                },
                "mcc": {
                    "type": "long"
                },
                "net": {
                    "type": "long"
                },
                "operator": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "radio": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                },
                "range": {
                    "type": "long"
                },
                "status": {
                    "type": "text",
                    "fields": {
                        "keyword": {
                            "type": "keyword",
                            "ignore_above": 256
                        }
                    }
                }
            }
        }
    }
}, function (err, resp, respcode) {
    console.log(err, resp, respcode);
});
