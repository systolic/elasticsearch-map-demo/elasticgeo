'use strict'

const _ = require('lodash')
const {Client} = require('@elastic/elasticsearch')
const geohash = require('ngeohash');

const client = new Client({node: 'http://localhost:9200'})

const indexName = 'cell-tower-counts'
const BATCH_SIZE = 10000

async function run() {
    console.log('Deleting existing index')
    await client.indices.delete({
        index: indexName,
        ignore_unavailable: true
    })

    console.log('Creating new index')
    await client.indices.create({
        index: indexName,
        body: {
            mappings: {
                properties: {
                    location: {
                        type: "geo_point"
                    },
                    doc_count: {
                        type: "integer"
                    },
                    precision: {
                        type: "integer"
                    }
                }
            },
            settings: {
                "number_of_shards": "4",
                "index.search.slowlog.threshold.query.warn": "1ms",
                "index.search.slowlog.threshold.query.info": "1ms",
                "index.search.slowlog.threshold.query.debug": "1ms",
                "index.search.slowlog.threshold.query.trace": "1ms",
                "index.search.slowlog.threshold.fetch.warn": "1ms",
                "index.search.slowlog.threshold.fetch.info": "1ms",
                "index.search.slowlog.threshold.fetch.debug": "1ms",
                "index.search.slowlog.threshold.fetch.trace": "1ms",
                "index.search.slowlog.level": "INFO"
            }
        }
    })

    const latitudeSize = 10

    for (let precision = 4; precision <= 6; ++precision) {
        console.log('Performing aggregation for precision ' + precision)

        let totalCount = 0

        for (let minLat = -180; minLat <= (180 - latitudeSize); minLat += latitudeSize) {
            const {body} = await client.search({
                index: 'cell-towers',
                body: {
                    size: 0,
                    aggregations: {
                        agg1: {
                            filter: {
                                geo_bounding_box: {
                                    location: {
                                        bottom_left: [minLat, -90],
                                        top_right: [minLat + latitudeSize, 90]
                                    }
                                }
                            },
                            aggregations: {
                                agg2: {
                                    geohash_grid: {
                                        field: 'location',
                                        precision: precision,
                                        size: 1000000
                                    }
                                }
                            }
                        }
                    }
                }
            })

            const buckets = body.aggregations.agg1.agg2.buckets
            let documents = []

            buckets.forEach((bucket, index) => {
                const point = geohash.decode(bucket.key)
//        const bbox = geohash.decode_bbox(bucket.key) // [minlat, minlon, maxlat, maxlon]

                documents.push({
                    location: bucket.key,
                    doc_count: bucket.doc_count,
                    precision: precision
                })

                if (documents.length % BATCH_SIZE === 0 || index === buckets.length - 1) {
                    const body = _.flatMap(documents, doc => [{index: {_index: indexName}}, doc])

                    client.bulk({refresh: true, body})

                    const bulkCount = documents.length
                    totalCount += documents.length
                    documents = []

                    console.log(`Wrote ${bulkCount} documents. Total written: ${totalCount}`)
                }
            })
        }

        console.log('Done with precision ' + precision)
    }
}

run().catch(console.log)