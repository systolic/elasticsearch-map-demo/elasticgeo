<StyledLayerDescriptor version="1.0.0"
                       xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
                       xmlns="http://www.opengis.net/sld"
                       xmlns:ogc="http://www.opengis.net/ogc"
                       xmlns:xlink="http://www.w3.org/1999/xlink"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>yellow-green-blue-heatmap</Name>
        <UserStyle>
            <Title>GeoHashGrid</Title>
            <Abstract>GeoHashGrid aggregation</Abstract>
            <FeatureTypeStyle>
                <Transformation>
                    <ogc:Function name="vec:GeoHashGrid">
                        <ogc:Function name="parameter">
                            <ogc:Literal>data</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>gridStrategy</ogc:Literal>
                            <ogc:Literal>Basic</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>pixelsPerCell</ogc:Literal>
                            <ogc:Literal>1</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputBBOX</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_bbox</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputWidth</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_width</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputHeight</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_height</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>scaleMin</ogc:Literal>
                            <ogc:Literal>0</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>scaleMax</ogc:Literal>
                            <ogc:Literal>1000</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>useLog</ogc:Literal>
                            <ogc:Literal>true</ogc:Literal>
                        </ogc:Function>
                    </ogc:Function>
                </Transformation>
                <Rule>
                    <RasterSymbolizer>
                        <Geometry>
                            <ogc:PropertyName>location</ogc:PropertyName>
                        </Geometry>
                        <Opacity>1</Opacity>
                        <ColorMap type="ramp">
                            <ColorMapEntry color="#081d58" quantity="0" opacity="0"/>
                            <ColorMapEntry color="#081d58" quantity="1"/>
                            <ColorMapEntry color="#253494" quantity="112"/>
                            <ColorMapEntry color="#225ea8" quantity="224"/>
                            <ColorMapEntry color="#1d91c0" quantity="336"/>
                            <ColorMapEntry color="#41b6c4" quantity="448"/>
                            <ColorMapEntry color="#7fcdbb" quantity="560"/>
                            <ColorMapEntry color="#c7e9b4" quantity="672"/>
                            <ColorMapEntry color="#edf8b1" quantity="784"/>
                            <ColorMapEntry color="#ffffd9" quantity="896"/>
                        </ColorMap>
                    </RasterSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>