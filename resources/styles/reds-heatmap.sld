<StyledLayerDescriptor version="1.0.0"
                       xsi:schemaLocation="http://www.opengis.net/sld StyledLayerDescriptor.xsd"
                       xmlns="http://www.opengis.net/sld"
                       xmlns:ogc="http://www.opengis.net/ogc"
                       xmlns:xlink="http://www.w3.org/1999/xlink"
                       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <NamedLayer>
        <Name>reds-heatmap</Name>
        <UserStyle>
            <Title>GeoHashGrid</Title>
            <Abstract>GeoHashGrid aggregation</Abstract>
            <FeatureTypeStyle>
                <Transformation>
                    <ogc:Function name="vec:GeoHashGrid">
                        <ogc:Function name="parameter">
                            <ogc:Literal>data</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>gridStrategy</ogc:Literal>
                            <ogc:Literal>Basic</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>pixelsPerCell</ogc:Literal>
                            <ogc:Literal>1</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputBBOX</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_bbox</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputWidth</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_width</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>outputHeight</ogc:Literal>
                            <ogc:Function name="env">
                                <ogc:Literal>wms_height</ogc:Literal>
                            </ogc:Function>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>scaleMin</ogc:Literal>
                            <ogc:Literal>0</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>scaleMax</ogc:Literal>
                            <ogc:Literal>1000</ogc:Literal>
                        </ogc:Function>
                        <ogc:Function name="parameter">
                            <ogc:Literal>useLog</ogc:Literal>
                            <ogc:Literal>true</ogc:Literal>
                        </ogc:Function>
                    </ogc:Function>
                </Transformation>
                <Rule>
                    <RasterSymbolizer>
                        <Geometry>
                            <ogc:PropertyName>location</ogc:PropertyName>
                        </Geometry>
                        <Opacity>1</Opacity>
                        <ColorMap type="ramp">
                            <ColorMapEntry color="#67000d" quantity="0" opacity="0"/>
                            <ColorMapEntry color="#67000d" quantity="1"/>
                            <ColorMapEntry color="#a50f15" quantity="200"/>
                            <ColorMapEntry color="#cb181d" quantity="300"/>
                            <ColorMapEntry color="#ef3b2c" quantity="400"/>
                            <ColorMapEntry color="#fb6a4a" quantity="500"/>
                            <ColorMapEntry color="#fc9272" quantity="600"/>
                            <ColorMapEntry color="#fcbba1" quantity="700"/>
                            <ColorMapEntry color="#fee0d2" quantity="800"/>
                            <ColorMapEntry color="#fff5f0" quantity="900"/>
                        </ColorMap>
                    </RasterSymbolizer>
                </Rule>
            </FeatureTypeStyle>
        </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>